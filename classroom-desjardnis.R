library("lme4")

classroom <- read.csv("http://www-personal.umich.edu/~bwest/classroom.csv")

correct.mod <- lmer(mathgain ~ (1 | schoolid/classid), data = classroom)
summary(correct.mod)

class.mod <- lmer(mathgain ~ (1 | schoolid:classid), data = classroom)
summary(class.mod)

anova(class.mod,correct.mod,refit=TRUE)
