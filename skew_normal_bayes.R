library("sn") 
source("DBDA2E-utilities.R") # for diagMCMC(), openGraph(), etc.!
require(runjags) # should already be loaded from DBDA2E-utilities.R
require(rjags)   # should already be loaded from DBDA2E-utilities.R

MCMCdiagnostics <- function(coda_samples, 
                            base_file_name, file_format="png",
                            parameters=c("scale","locat","skew")){
  
  # prompt the user to continue before moving to next page of output
  require(grDevices)
  default_ask <- devAskNewPage()
  on.exit(devAskNewPage(default_ask))
  devAskNewPage(ask = TRUE)
  
  mcmcMat <- as.matrix( coda_samples )
  
  # diagnostics for model parameters
  for(p in parameters){
    diagMCMC(codaObject=coda_samples , parName=p , 
             saveName=base_file_name , saveType=file_format )
  }
  # Examine correlation of parameters in posterior distribution:
  openGraph()
  pairs( mcmcMat , col="skyblue" )
  saveGraph( file=paste0(base_file_name,"-Pairs") , type=file_format )
  
  # plot data with posterior predictions
  openGraph(height=4,width=7)
  histInfo = hist( dataList$y , probability=TRUE , breaks=31 ,
                   xlab="Data Value" , main="Data with Posterior Pred." ,
                   col="gray" , border="white" )
  nCurves = 30
  curveIdxVec = round(seq(1,nrow(mcmcMat),length=nCurves))
  xComb = seq( min(histInfo$breaks) , max(histInfo$breaks) , length=501 )
  for ( curveIdx in curveIdxVec ) {
    lines( xComb , dsn( xComb , 
                        xi=mcmcMat[curveIdx,"locat"] , 
                        omega=mcmcMat[curveIdx,"scale"] , 
                        alpha=mcmcMat[curveIdx,"skew"] ) ,
           col="skyblue" )
  }
  saveGraph( file=paste0(base_file_name,"-Data-PostPred") , type=file_format )
}

# Create random data from skew-normal distribution:
generateData <- function(locat, scale, skew, N=2000,seed=47405){
  set.seed(seed)
  y = rsn( N , xi=locat , omega=scale , alpha=skew )
  # Scaling constant for use in JAGS Bernoulli ones trick:
  dsnMax = 1.1*max(dsn( seq(-10,10,length=1001) , xi=locat,omega=scale,alpha=skew ))
  # Assemble data for JAGS:
  list(
    y = y ,
    N = N ,
    ones = rep(1,length(y)) ,
    C = dsnMax # constant for keeping scaled dsn < 1
  )
}

# Generate data:
locat <- 1
scale <- 2
skew <- 3
dataList <- generateData(locat,scale,skew)

# Construct file name root based on generating parameter values:
fileNameRoot = paste0("SkewNormalPlay","-",locat,"-",scale,"-",skew)
saveType = "png"

# Define the JAGS model using Bernoulli ones trick
# as explained in DBDA2E Section 8.6.1 pp. 214-215.
modelString = "
model {
for ( i in 1:N ) {
dsn[i] <- ( (2/scale) 
* dnorm( (y[i]-locat)/scale , 0 , 1 ) 
* pnorm( skew*(y[i]-locat)/scale , 0 , 1 ) )
spy[i] <- dsn[i] / C
ones[i] ~ dbern( spy[i] )
}
scale ~ dgamma(1.105,0.105)
locat ~ dnorm(0,1/10^2)
skew ~ dnorm(0,1/10^2)
}
" # close quote for modelString
writeLines( modelString , con="TEMPmodel.txt" )

# Run the chains:
runJagsOut <- run.jags( method="parallel" ,
                        model="TEMPmodel.txt" , 
                        monitor=c("scale","locat","skew") , 
                        data=dataList ,  
                        #inits=initsList , 
                        n.chains=3 ,
                        adapt=500 ,
                        burnin=500 , 
                        sample=ceiling(12000/3) , 
                        thin=5 ,
                        summarise=FALSE ,
                        plots=FALSE )



codaSamples = as.mcmc.list( runJagsOut ) # from rjags package
suffix <- sprintf("jags_samples_scale%.2f_location%.2f_skew%.2f.rds",scale,locat,skew)
saveRDS(codaSamples , file=paste0(fileNameRoot,suffix))

fname <- sprintf("skew_jags_scale%.2f_location%.2f_skew%.2f",scale,locat,skew)
MCMCdiagnostics(codaSamples,fname,saveType)

library(rstan)
rstan_options(auto_write = TRUE)
options(mc.cores = parallel::detectCores(logical=FALSE))

stancustom  <- "
  // custom log density function based on JAGS code above
  // note: this skips the 'bernoulli' trick bit and doesn't normalize!
  // the lack of normalization doesn't seem to be an issue for stan
  functions{
    real skewnorm_log(real[] x, real xi, real omega, real alpha){
      real lprob[num_elements(x)];
      real trans_x[num_elements(x)];
      real lscale;
      
      lscale = log(2 / omega);
      for (i in 1:num_elements(x)){
        trans_x[i] = (x[i] - xi) / omega;
        lprob[i] = lscale + normal_lpdf(trans_x[i] | 0 , 1) + normal_lcdf(alpha*trans_x[i]| 0, 1);
      }
      return sum(lprob);
    }
  }
  data {
    int<lower=0> N; // number of data points
    real y[N]; // observed data
  }
  parameters {
    real<lower=0> scale;
    real locat;
    real skew;
    
  }
  model{
    scale ~ gamma(1.105,0.105);
    // stan uses sd, not precision
    // tau=1/10^2 -> var=10^2  -> sd=10
    locat ~ normal(0,10);
    skew ~ normal(0,10);
    
    y ~ skewnorm(locat,scale,skew);
}
"

stancustom.model <- stan(model_name="stan_custom_skew",
                        model_code=stancustom,
                        data=dataList,
                        # match the sampling from JAGS
                        # NB this isn't necessarily advisable
                        # 21k - 1k (warmup) = 20k
                        # 20k / 5 = 4k samples/chain post-thinning
                        # 4k * 3 = 12k samples total
                        iter=21000,warmup=1000,chains=3,thin=5) 
codaSamples <- As.mcmc.list( stancustom.model )
fname <- sprintf("skew_stancustom_scale%.2f_location%.2f_skew%.2f",scale,locat,skew)
MCMCdiagnostics(codaSamples,fname,saveType)


stannative <- "
  data {
    int<lower=0> N; // number of data points
    real y[N]; // observed data
  }
  parameters {
    real<lower=0> scale;
    real locat;
    real skew;
    
  }
  model{
    scale ~ gamma(1.105,0.105);
    // stan uses sd, not precision
    // tau=1/10^2 -> var=10^2  -> sd=10
    locat ~ normal(0,10);
    skew ~ normal(0,10);
    
    y ~ skew_normal(locat,scale,skew);
}
"

stannative.model <- stan(model_name="stan_native_skew",
                        model_code=stannative,
                        data=dataList,
                        # match the sampling from JAGS
                        # NB this isn't necessarily advisable 
                        iter=21000,warmup=1000,chains=3,thin=5) 



codaSamples <- As.mcmc.list( stannative.model )
fname <- sprintf("skew_stannative_scale%.2f_location%.2f_skew%.2f",scale,locat,skew)
MCMCdiagnostics(codaSamples,fname,saveType)


# try it all again with skew=0
skew <- 0
dataList <- generateData(locat,scale,skew)

# jags
runJagsOut <- run.jags( method="parallel" ,
                        model="TEMPmodel.txt" , 
                        monitor=c("scale","locat","skew") , 
                        data=dataList ,  
                        #inits=initsList , 
                        n.chains=3 ,
                        adapt=500 ,
                        burnin=500 , 
                        sample=ceiling(12000/3) , 
                        thin=5 ,
                        summarise=FALSE ,
                        plots=FALSE )



codaSamples = as.mcmc.list( runJagsOut ) # from rjags package
suffix <- sprintf("jags_samples_scale%.2f_location%.2f_skew%.2f.rds",scale,locat,skew)
saveRDS(codaSamples , file=paste0(fileNameRoot,suffix))

fname <- sprintf("skew_jags_scale%.2f_location%.2f_skew%.2f",scale,locat,skew)
MCMCdiagnostics(codaSamples,fname,saveType)

# custom stan
stancustom.model <- stan(model_name="stan_custom_skew",
                         model_code=stancustom,
                         data=dataList,
                         # match the sampling from JAGS
                         # NB this isn't necessarily advisable 
                         iter=21000,warmup=1000,chains=3,thin=5) 
codaSamples <- As.mcmc.list( stancustom.model )
fname <- sprintf("skew_stancustom_scale%.2f_location%.2f_skew%.2f",scale,locat,skew)
MCMCdiagnostics(codaSamples,fname,saveType)

# native stan
stannative.model <- sampling(get_stanmodel(stannative.model),
                             data=dataList,
                             # match the sampling from JAGS
                             # NB this isn't necessarily advisable 
                             iter=21000,warmup=1000,chains=3,thin=5) 
codaSamples <- As.mcmc.list( stannative.model )
fname <- sprintf("skew_stannative_scale%.2f_location%.2f_skew%.2f",scale,locat,skew)
MCMCdiagnostics(codaSamples,fname,saveType)
